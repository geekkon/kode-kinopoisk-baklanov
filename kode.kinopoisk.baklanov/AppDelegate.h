//
//  AppDelegate.h
//  kode.kinopoisk.baklanov
//
//  Created by Dim on 10.10.16.
//  Copyright © 2016 Dmitriy Baklanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

