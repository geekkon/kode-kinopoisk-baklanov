//
//  main.m
//  kode.kinopoisk.baklanov
//
//  Created by Dim on 10.10.16.
//  Copyright © 2016 Dmitriy Baklanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
